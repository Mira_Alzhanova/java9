import human.Human;

import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaDemo {
    public static final Function<String, Integer> getLength = String::length;
    public static final Function<String, Character> getFirstSymbol = string -> ("".equals(string))? null: string.charAt(0);
    public static final Function<String, Boolean> doesStringContainSpaces = string -> {
        if ("".equals(string)) {
            return false;
        }
        return string.indexOf(' ') > -1;
    };
    public static final Function<String, Integer> amountOfWordsInString = string -> {
        if(string.equals("")){
            return 0;
        }
        String[] res = string.split(",");
        return res.length;
    };
    public static final Function<Human, Integer> getHumanAge = Human::getAge;
    public static final BiFunction<Human, Human, Boolean> isSecondNamesEqual = (firstHuman, secondHuman) -> firstHuman.getSecondName().equals(secondHuman.getSecondName());
    public static final Function<Human, String> getFullName = human -> human.getFirstName() + " " + human.getSecondName() + " " + human.getMiddleName();
    public static final Function<Human, Human> makeHumanOneYearOlder = human -> human.incrementAge(human);
    public static final OperationWithThreePeople<Boolean> checkAllYounger = ((firstHuman, secondHuman, thirdHuman, maxAge) ->
            firstHuman.getAge() < maxAge && secondHuman.getAge() < maxAge && thirdHuman.getAge() < maxAge);
}

interface OperationWithThreePeople<T>{
    T calculate(Human firstHuman, Human secondHuman, Human thirdHuman, int maxAge);
}

