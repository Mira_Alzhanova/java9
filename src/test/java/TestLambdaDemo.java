import human.Human;
import human.Human;
import human.Sex;
import human.Student;
import org.junit.Assert;
import org.junit.Test;

public class TestLambdaDemo {

    @Test
    public void testGetLength(){
        Assert.assertEquals(0, (int)LambdaRunner.run(LambdaDemo.getLength, ""));
        Assert.assertEquals(3, (int)LambdaRunner.run(LambdaDemo.getLength, "abc"));
        Assert.assertEquals(1, (int)LambdaRunner.run(LambdaDemo.getLength, "a"));
    }

    @Test
    public void testGetFirstSymbol(){
        Assert.assertEquals('a', (char)LambdaRunner.run(LambdaDemo.getFirstSymbol, "abc"));
        Assert.assertEquals('.', (char)LambdaRunner.run(LambdaDemo.getFirstSymbol, ".bc"));
    }

    @Test
    public void testDoesStringConsistSpaces(){
        Assert.assertTrue(LambdaRunner.run(LambdaDemo.doesStringContainSpaces, " "));
        Assert.assertTrue(LambdaRunner.run(LambdaDemo.doesStringContainSpaces, " a b c "));
    }

    @Test
    public void testAmountOfWordsInString(){
        Assert.assertEquals(0, (int)LambdaRunner.run(LambdaDemo.amountOfWordsInString, ""));
        Assert.assertEquals(1, (int)LambdaRunner.run(LambdaDemo.amountOfWordsInString, "one"));
        Assert.assertEquals(2, (int)LambdaRunner.run(LambdaDemo.amountOfWordsInString, "one, two"));
    }

    @Test
    public void testGetHumanAge(){
        Human human = new Human(Sex.Female, "Jane", "Huston", "Third", 23);
        Student student = new Student(human, "Harvard", "Biology Faculty", "Programmer");
        Assert.assertEquals(23, (int)LambdaRunner.run(LambdaDemo.getHumanAge, student));
    }

    @Test
    public void testIsSecondNamesEqual(){
        Human human = new Human(Sex.Female, "Jane", "Huston", "Third", 23);
        Human secondHuman = new Human(Sex.Female, "Jane", "Jonson", "Third", 23);
        Student student = new Student(human, "Harvard", "Biology Faculty", "Programmer");
        Assert.assertTrue(LambdaRunner.run(LambdaDemo.isSecondNamesEqual, human, student));
        Assert.assertFalse(LambdaRunner.run(LambdaDemo.isSecondNamesEqual, human, secondHuman));
    }

    @Test
    public void testGetFullName(){
        Human human = new Human(Sex.Female, "Jane", "Huston", "Third", 23);
        Assert.assertEquals("Jane Huston Third", LambdaRunner.run(LambdaDemo.getFullName, human));
    }

    @Test
    public void testMakeHumanOneYearOlder(){
        Human human = new Human(Sex.Female, "Jane", "Huston", "Third", 23);
        Human expected = new Human(Sex.Female, "Jane", "Huston", "Third", 24);
        Assert.assertEquals(expected, LambdaRunner.run(LambdaDemo.makeHumanOneYearOlder, human));
    }

    @Test
    public void testCheckAllYounger(){
        Human firstHuman = new Human(Sex.Female, "Jane", "Huston", "First", 23);
        Human secondHuman = new Human(Sex.Female, "Jane", "Huston", "Second", 23);
        Human thirdHuman = new Human(Sex.Female, "Jane", "Huston", "Third", 23);
        Assert.assertTrue(LambdaRunner.checkAllYounger(LambdaDemo.checkAllYounger, firstHuman, secondHuman, thirdHuman, 34));
    }
}

